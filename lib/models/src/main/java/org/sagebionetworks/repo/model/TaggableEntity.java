package org.sagebionetworks.repo.model;

/**
 * Whether the entity can be tagged.
 *
 * @author ewu
 */
public interface TaggableEntity {}
